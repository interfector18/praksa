export default interface Product {
    img: string
    name: string
    price: string
}